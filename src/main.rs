use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use clap::{ArgGroup, StructOpt};
use color_eyre::eyre::WrapErr as _;
use iroha_crypto::{Algorithm, KeyGenConfiguration, KeyPair, PrivateKey};
use rocksdb::{Options, DB};
use serde::{Deserialize, Serialize};
use serde_json::{from_str as json_parse, to_string as json_encode};
use std::io::{stdout, BufWriter, Write};
use std::process::Command;

pub type Outcome = color_eyre::Result<()>;

// ---- VARS -------
const DB_PATH: &'static str = "retimpresa_blockchain_midlle_layer";
const IROHA_CLIENT_COMMAND: &'static str = "./iroha_cli/iroha_client_cli";
// --- ENDS OF VARS ----

#[derive(Deserialize, Serialize)]
struct IdentityKey {
    pub public_key: String,
    pub private_key: String,
}

impl IdentityKey {
    pub fn generate_keys(seed: String) -> IdentityKey {
        let gen = Self::key_pair(Some(seed)).unwrap();
        IdentityKey {
            public_key: format!("{}", gen.public_key()),
            private_key: format!("{}", gen.private_key()),
        }
    }

    pub fn key_pair(seed: Option<String>) -> color_eyre::Result<KeyPair> {
        let key_gen_configuration =
            KeyGenConfiguration::default().with_algorithm(Algorithm::Ed25519);
        let private_key: Option<String> = None;
        let keypair: KeyPair = (seed).map_or_else(
            || -> color_eyre::Result<_> {
                private_key.map_or_else(
                    || {
                        KeyPair::generate_with_configuration(key_gen_configuration.clone())
                            .wrap_err("failed to generate key pair")
                    },
                    |private_key| {
                        let private_key = PrivateKey::from_hex(Algorithm::Ed25519, &private_key)
                            .wrap_err("Failed to decode private key")?;
                        KeyPair::generate_with_configuration(
                            key_gen_configuration.clone().use_private_key(private_key),
                        )
                        .wrap_err("Failed to generate key pair")
                    },
                )
            },
            |seed| -> color_eyre::Result<_> {
                KeyPair::generate_with_configuration(
                    key_gen_configuration
                        .clone()
                        .use_seed(seed.as_bytes().into()),
                )
                .wrap_err("Failed to generate key pair")
            },
        )?;
        Ok(keypair)
    }
}

#[derive(Deserialize, Serialize)]
struct EntityStore {
    pub id: String,
    pub keys: IdentityKey,
}

impl EntityStore {
    pub fn get(id: String) -> Option<EntityStore> {
        // let db = DB::open_default().unwrap();
        let db = sled::open(DB_PATH).expect("Error while opening/creating db");
        match db.get(id.as_bytes()) {
            Ok(Some(value)) => {
                let _keys: IdentityKey =
                    json_parse(&String::from_utf8(value.to_vec()).unwrap()).unwrap();
                return Some(EntityStore { id, keys: _keys });
            }
            Ok(None) => {
                let _keys = IdentityKey::generate_keys(id.clone());
                let _res = db.insert(id.clone(), json_encode(&_keys).unwrap().as_bytes());

                // Iroha smart contract trx to Blockchain
                println!("Executing: {}", &format!("{} account register --id '{}@retimpresa' --key '{}'", IROHA_CLIENT_COMMAND, id, _keys.public_key));
                let bash_out = Command::new("sh")
                    .arg("-c")
                    .arg(&format!("{} account register --id '{}@retimpresa' --key '{}'", IROHA_CLIENT_COMMAND, id, _keys.public_key))
                    .output()
                    .expect("Failed to execute iroha transaction... Check if blockchain nodes are alive");
                let display = String::from_utf8(bash_out.stdout).unwrap();
                println!("Result: {}", display);
                
                return Some(EntityStore { id, keys: _keys });
            }
            Err(e) => println!("operational problem encountered: {}", e),
        }
        None
    }
}

#[get("/{entity}")]
async fn keys(entity: web::Path<String>) -> impl Responder {
    let result: Option<EntityStore> = EntityStore::get(entity.to_string());
    match result {
        Some(k) => web::Json(k),
        None => {
            web::Json(json_parse("{'err': 'Unable to find or create keys for entity' }").unwrap())
        }
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().service(keys))
        .bind(("127.0.0.1", 6900))?
        .run()
        .await
}
