[package]
name = "iroha_data_model"
version = "2.0.0-pre-rc.6"
authors = ["Iroha 2 team <https://github.com/orgs/soramitsu/teams/iroha2>"]
edition = "2021"
description = "Iroha uses a simple data model made up of domains, peers, accounts, assets, signatories, and permissions. This library contains basic data model structures."
readme = "README.md"
homepage = "https://github.com/hyperledger/iroha/tree/iroha2-dev"
repository = "https://github.com/hyperledger/iroha/tree/iroha2-dev"
license = "Apache-2.0"
keywords = ["crypto", "blockchain", "ledger", "iroha", "model"]
categories = ["cryptography::cryptocurrencies", "api-bindings"]

[badges]
is-it-maintained-issue-resolution = { repository = "https://github.com/hyperledger/iroha" }
is-it-maintained-open-issues = { repository = "https://github.com/hyperledger/iroha" }
maintenance = { status = "actively-developed" }

[features]
default = ["std"]
# Enable static linkage of the rust standard library.
# Disabled for WASM interoperability, to reduce the binary size.
# Please refer to https://docs.rust-embedded.org/book/intro/no-std.html
std = ["iroha_macro/std", "iroha_version/std", "iroha_version/warp", "iroha_crypto/std", "iroha_data_primitives/std", "thiserror", "strum/std", "dashmap", "tokio"]
# Expose FFI API to facilitate dynamic linking
ffi_api = ["iroha_ffi", "iroha_crypto/ffi_api"]

# Expose API for mutating structures (Internal use only)
mutable_api = []

[dependencies]
iroha_data_primitives = { path = "primitives", version = "=2.0.0-pre-rc.6", default-features = false }
iroha_crypto = { path = "../crypto", version = "=2.0.0-pre-rc.6", default-features = false }
iroha_macro = { path = "../macro", version = "=2.0.0-pre-rc.6", default-features = false }
iroha_version = { path = "../version", version = "=2.0.0-pre-rc.6", default-features = false, features = ["derive", "json", "scale"] }
iroha_schema = { path = "../schema", version = "=2.0.0-pre-rc.6" }
iroha_ffi = { path = "../ffi", version = "=2.0.0-pre-rc.6", optional = true }

dashmap = { version = "4.0", optional = true}
tokio = { version = "1.6.0", features = ["sync", "rt-multi-thread"], optional = true}
parity-scale-codec = { version = "2.3.1", default-features = false, features = ["derive"] }
derive_more = { version = "0.99.16", default-features = false, features = ["display", "constructor", "from_str"] }
serde = { version = "1.0", default-features = false, features = ["derive"] }
serde_json = { version = "1.0.59", default-features = false }
warp = { version = "0.3", default-features = false, optional = true }
thiserror = { version = "1.0.28", optional = true }
getset = "0.1.2"
strum = { version = "0.24.0", default-features = false, features = ["derive"] }

[dev-dependencies]
iroha_core = { path = "../core", version = "=2.0.0-pre-rc.6" }
iroha_client = { path = "../client", version = "=2.0.0-pre-rc.6" }
iroha = { path = "../cli" }
hex = { version = "0.4.0", default-features = false, features = ["alloc", "serde"] }

test_network = { path = "../core/test_network", version = "=2.0.0-pre-rc.6" }

tokio = { version = "1.6.0", features = ["rt", "rt-multi-thread"]}
trybuild = "1.0.53"
criterion = "0.3"

[[bench]]
name = "time_event_filter"
harness = false
