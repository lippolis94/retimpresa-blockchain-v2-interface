# RetImpresa Iroha interface Docs

## How it works

Iroha v2, è stato messo in produzione con 4 nodi (redundancy & data persistance) che runnano su docker.
Per osservarne lo stato sarà necessario lanciare `docker ps`.

Qualora la macchina dovesse essere riavviata o per qualunque ragione questi container dovessero interrompersi basterà riavviarli lanciando il seguente comando: `cd /home/zwan/iroha-blockchain/iroha && docker-compose up` con i privilegi sudo in quanto iroha richiede i permessi per poter effettuare lo spawn di thread indipendenti sulla macchina.

La blockchain riprende dal punto in cui aveva interrotto, quindi non ci potranno più essere perdite di dati in quanto in questa versione è aggiornata è possibile salvare il block storage, al di fuori dell'attività del container.

## Blockchain Interface

Per interfacciare la blockchain con l'applicativo frontend è stata creata un'interfaccia API in Rust (linguaggio supportato nativamente da Iroha2), che è rappresentata da questa repository. La logica è all'interno della cartella `src` e prevede l'esposizione di un singolo endpoint su rete locale e porta 6900 (http://127.0.0.1:6900).

Per compilare l'interfaccia sarà necessario installare il toolkit di sviluppo rust, clang ed llvm, e lanciare `cargo build --release`. Ad ogni modo in questa repository è presente il comppilato per OS linux-musl (Centos7) nella cartella /target/release/retimpresa-blockchain-interface.

Il compilato è un binario che deve essere in esecuzione perenne per esporre l'API e richiede l'utilizzo del client iroha (sempre in Rust e sempre compilato all'interno della cartella di esecuzione) per poter chiamare le funzioni di registrazione degli account sulla blockchain.

In caso di riavvio sarà necessario lanciare il compilato con il metodo nohup e in background: `nohup ./retimpresa-blockchain-interface &` o tramite l'utilzzo di un gestore di processi come `immortal`.

L'api funziona in qeuesto modo: la path passata, (eg. http://127.0.0.1:6900/impresa_34750134570), genera qualora non esistesse già una coppia di chiavi (pubblica e privata) che vengono registrate tramite uno smart contract all'interno della blockchain. In questo modo qualunque tipo di entità potrà essere registrato all'interno della blockchain lasciando spazio a nuovi ed ulteriori sviluppi. Nel file di stdout è possibile osservare la chiamata ad iroha e la conseguente risposta con la conferma dell'avvenuta transazione.

Per fare ciò è stato usato un database nosql embedded che fa da middle layer, cachando le "path" già generate e restituendo la chiave pubblica e privata.

Esempio

```
GET https://127.0.0.1:6900/rete_92384693847

{"id":"rete_92384693847","keys":{"public_key":"ed0120c1abd598d4ac8f2835281ba354de4263b303030c28640f92ee549f6702075ace","private_key":"810adf5b859bbe5f668bfb96431c4d821cf08dbd3adc7272c73c634ea67dc621c1abd598d4ac8f2835281ba354de4263b303030c28640f92ee549f6702075ace"}}
```

Il db middle layer è salvato all'interno della cartella `retimpresa_blockchain_middle_layer`.
